package impl.treelinearizers;

import api.Tree;
import api.utils.NodePath;
import exception.TreeInsertionException;
import impl.childtrees.DefaultMutableChildTree;
import impl.nodepaths.ImmutableNodePath;
import impl.nodes.ImmutableNode;
import impl.trees.DefaultImmutableTree;
import impl.treevalues.IntegerTreeValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by chief on 03.07.17.
 */
class CompactTreeLinearizerIT {
    Tree<Integer> tree;

    @BeforeEach
    void setUp() throws TreeInsertionException {
        DefaultMutableChildTree<Integer> chtree = new DefaultMutableChildTree<>();
        DefaultMutableChildTree<Integer> ch1tree = new DefaultMutableChildTree<>();
        DefaultMutableChildTree<Integer> ch1ch2tree = new DefaultMutableChildTree<>();
        DefaultMutableChildTree<Integer> ch3tree = new DefaultMutableChildTree<>();
        tree = new DefaultImmutableTree<>(new ImmutableNode<>(new IntegerTreeValue(1), chtree, new ImmutableNodePath<>(1)));
        chtree.setMainTree(tree);
        chtree.addNode(new ImmutableNode<>(new IntegerTreeValue(1), tree.getTreeRoot(), ch1tree, new ImmutableNodePath<>(Arrays.asList(1, 1))));
        chtree.addNode(new ImmutableNode<>(new IntegerTreeValue(2), tree.getTreeRoot(), new DefaultMutableChildTree<>(tree), new ImmutableNodePath<>(Arrays.asList(1, 2))));
        chtree.addNode(new ImmutableNode<>(new IntegerTreeValue(3), tree.getTreeRoot(), ch3tree, new ImmutableNodePath<>(Arrays.asList(1, 3))));

        ch1tree.setMainTree(tree);
        ch1tree.addNode(new ImmutableNode<>(new IntegerTreeValue(1), chtree.getParent(), new DefaultMutableChildTree<>(tree), new ImmutableNodePath<>(Arrays.asList(1, 1, 1))));
        ch1tree.addNode(new ImmutableNode<>(new IntegerTreeValue(2), chtree.getParent(), ch1ch2tree, new ImmutableNodePath<>(Arrays.asList(1, 1, 2))));

        ch1ch2tree.setMainTree(tree);
        ch1ch2tree.addNode(new ImmutableNode<>(new IntegerTreeValue(1), ch1tree.getParent(), new DefaultMutableChildTree<>(tree), new ImmutableNodePath<>(Arrays.asList(1, 2, 1))));

        ch3tree.setMainTree(tree);
        ch3tree.addNode(new ImmutableNode<>(new IntegerTreeValue(1), chtree.getParent(), new DefaultMutableChildTree<>(tree), new ImmutableNodePath<>(Arrays.asList(1, 3, 1))));
        ch3tree.addNode(new ImmutableNode<>(new IntegerTreeValue(2), chtree.getParent(), new DefaultMutableChildTree<>(tree), new ImmutableNodePath<>(Arrays.asList(1, 3, 2))));
        ch3tree.addNode(new ImmutableNode<>(new IntegerTreeValue(3), chtree.getParent(), new DefaultMutableChildTree<>(tree), new ImmutableNodePath<>(Arrays.asList(1, 3, 3))));

    }

    @Test
    void linearizeDefaultMutableTree() throws TreeInsertionException {

        Set<NodePath<Integer>> nodePaths = new HashSet<>(new CompactTreeLinearizer<Integer>().linearize(tree));
        ImmutableNodePath<Integer> nodePath1 = new ImmutableNodePath<>(Arrays.asList(1, 1, 1));
        ImmutableNodePath<Integer> nodePath2 = new ImmutableNodePath<>(Arrays.asList(1, 1, 2, 1));
        ImmutableNodePath<Integer> nodePath3 = new ImmutableNodePath<>(Arrays.asList(1, 2));
        ImmutableNodePath<Integer> nodePath4 = new ImmutableNodePath<>(Arrays.asList(1, 3, 1));
        ImmutableNodePath<Integer> nodePath5 = new ImmutableNodePath<>(Arrays.asList(1, 3, 2));
        ImmutableNodePath<Integer> nodePath6 = new ImmutableNodePath<>(Arrays.asList(1, 3, 3));
        HashSet<ImmutableNodePath<Integer>> expected = new HashSet<>(Arrays.asList(
                nodePath1,
                nodePath2,
                nodePath3,
                nodePath4,
                nodePath5,
                nodePath6
        ));
        assertTrue(nodePaths.contains(nodePath1));
        assertTrue(nodePaths.contains(nodePath2));
        assertTrue(nodePaths.contains(nodePath3));
        assertTrue(nodePaths.contains(nodePath4));
        assertTrue(nodePaths.contains(nodePath5));
        assertTrue(nodePaths.contains(nodePath6));
        assertEquals(expected, nodePaths);
    }



}