package impl.treelinearizers;

import api.ChildTree;
import api.Node;
import api.Tree;
import api.utils.NodePath;
import impl.childtrees.DefaultMutableChildTree;
import impl.nodepaths.ImmutableNodePath;
import impl.nodes.ImmutableNode;
import impl.trees.DefaultImmutableTree;
import impl.treevalues.IntegerTreeValue;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Created by chief on 04.07.17.
 */
class FullTreeLinearizerIT {
    @Test
    void buildNodePaths() {
        ImmutableNode<Integer> node = new ImmutableNode<>(new IntegerTreeValue(1), new DefaultMutableChildTree<>(), new ImmutableNodePath<>(1));
        Tree<Integer> tree = new DefaultImmutableTree<>(node);

        Map<NodePath<Node<Integer>>, ChildTree<Integer>> nodePathChildTreeMap = new FullTreeLinearizer<Integer>().buildNodePaths(tree);
        Map<NodePath<Node<Integer>>, ChildTree<Integer>> expected = new HashMap<>();
        expected.put(new ImmutableNodePath<>(node), new DefaultMutableChildTree<>());
        assertEquals(expected, nodePathChildTreeMap);
    }
}