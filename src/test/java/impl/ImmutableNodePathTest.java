package impl;

import api.utils.NodePath;
import impl.nodepaths.ImmutableNodePath;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by chief on 04.07.17.
 */
class ImmutableNodePathTest {
    NodePath<Integer> nodePath1;
    NodePath<Integer> nodePath2;
    NodePath<Integer> nodePath3;

    @BeforeEach
    void setUp() {
        nodePath1 = new ImmutableNodePath<>(Arrays.asList(
                1, 2, 1
        ));
        nodePath2 = new ImmutableNodePath<>(Arrays.asList(
                1, 2, 4
        ));
        nodePath3 = new ImmutableNodePath<>(Arrays.asList(
                1, 2, 1
        ));
    }

    @Test
    void getNodes() {
        assertEquals(new ArrayList<>(Arrays.asList(
                1,2,1
        )), new ArrayList<>(nodePath1.getNodes()));
        assertEquals(new ArrayList<>(Arrays.asList(
                1,2,4
        )), new ArrayList<>(nodePath2.getNodes()));
        assertEquals(new ArrayList<>(Arrays.asList(
                1,2,1
        )), new ArrayList<>(nodePath3.getNodes()));
    }

    @Test
    void testEquals() {
        assertFalse(nodePath1.equals(nodePath2));
        assertTrue(nodePath1.equals(nodePath3));
        assertFalse(nodePath2.equals(nodePath1));
        assertFalse(nodePath2.equals(nodePath3));
        assertTrue(nodePath3.equals(nodePath1));
        assertFalse(nodePath3.equals(nodePath2));
    }

    @Test
    void testHashCode() {
        assertNotEquals(nodePath1.hashCode(), nodePath2.hashCode());
        assertEquals(nodePath1.hashCode(), nodePath3.hashCode());
    }

}