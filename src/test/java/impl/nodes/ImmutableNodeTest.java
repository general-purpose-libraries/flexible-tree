package impl.nodes;

import api.TreeValue;
import impl.childtrees.DefaultMutableChildTree;
import impl.nodepaths.ImmutableNodePath;
import impl.treevalues.IntegerTreeValue;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Created by chief on 06.07.17.
 */
class ImmutableNodeTest {
    @Test
    void getValue() {
        ImmutableNode<Integer> node = new ImmutableNode<>(new IntegerTreeValue(1), new DefaultMutableChildTree<>(), new ImmutableNodePath<>());
        assertTrue(node.getValue() instanceof TreeValue);
        assertEquals(new IntegerTreeValue(1), node.getValue());
        assertEquals(1, (int) node.getValue().getValue());
    }

}