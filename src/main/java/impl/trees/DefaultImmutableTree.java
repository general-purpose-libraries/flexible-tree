package impl.trees;

import api.Node;
import api.Tree;
import impl.nodes.ImmutableNode;

import javax.swing.event.TreeModelListener;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by chief on 04.07.17.
 */
public class DefaultImmutableTree<T> implements Tree<T> {
    private final Node<T> node;
    private final Collection<TreeModelListener> listeners = new ArrayList<>();

    public DefaultImmutableTree(ImmutableNode<T> node) {
        assert node != null;
        this.node = node;
    }

    @Override
    public Node<T> getTreeRoot() {
        return node;
    }

    @Override
    public Collection<TreeModelListener> getListeners() {
        return new ArrayList<>(listeners);
    }

    @Override
    public void addTreeModelListener(TreeModelListener treeModelListener) {
        assert treeModelListener != null;
        listeners.add(treeModelListener);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener treeModelListener) {
        assert treeModelListener != null;
        listeners.remove(treeModelListener);
    }
}
