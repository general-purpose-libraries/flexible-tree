package impl.treelinearizers;

import api.ChildTree;
import api.Node;
import api.utils.NodePath;
import api.utils.TreeLinearizer;
import impl.nodepaths.ImmutableNodePath;

import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Builds a collection of immutable NodePaths. It contains all the node paths the tree including
 * nodes that are not leafs
 */
public class FullTreeLinearizer<T> implements TreeLinearizer<T, T> {

    @Override
    public Predicate<Map.Entry<NodePath<Node<T>>, ChildTree<T>>> getFilter() {
        return nodePathChildTreeEntry -> {
            boolean empty = nodePathChildTreeEntry.getValue().getNodes().isEmpty();
            return empty;
        };
    }

    @Override
    public NodeConverter<T, T> getConverter() {
        return nodePath -> {
            NodePath<Node<T>> path = nodePath.getKey();
            List<T> nodes = path.getNodes().parallelStream().map(node -> node.getValue().getValue()).collect(Collectors.toList());
            return new ImmutableNodePath<>(nodes);
        };
    }
}
