package impl.childtrees;

import api.Node;
import api.Tree;
import exception.TreeInsertionException;
import impl.nodes.ImmutableNode;

/**
 * Created by chief on 04.07.17.
 */
public class MutableChildTreeWithImmutableChilds<T> extends DefaultMutableChildTree<T> {
    public MutableChildTreeWithImmutableChilds(Tree<T> mainTree) {
        super(mainTree);
    }

    @Override
    public void addNode(Node<T> node) throws TreeInsertionException {
        if (!(node instanceof ImmutableNode
                && ((ImmutableNode) node).getChildTree() instanceof MutableChildTreeWithImmutableChilds)){
            throw new IllegalArgumentException(String.format("This child tree only supports Nodes of type %s with child tree of type %s",
                    ImmutableNode.class.getSimpleName(), MutableChildTreeWithImmutableChilds.class.getSimpleName()));
        }
        super.addNode(node);
    }
}
