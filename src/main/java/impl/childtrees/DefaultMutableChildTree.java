package impl.childtrees;

import api.Node;
import api.Tree;
import api.utils.MutableChildTree;
import exception.NodeNotFoundException;
import exception.TreeInsertionException;
import exception.TreeRemovalException;

import java.util.ArrayList;
import java.util.List;

/**
 * Default implementation of a MutableTree
 */
public class DefaultMutableChildTree<T> implements MutableChildTree<T> {
    private Tree<T> mainTree;
    private Node<T> parent;
    private final ArrayList<Node<T>> nodes = new ArrayList<>();

    public DefaultMutableChildTree() {}

    public DefaultMutableChildTree(Tree<T> mainTree) {
        assert mainTree != null;
        this.mainTree = mainTree;
    }

    public void setMainTree(Tree<T> mainTree) {
        assert mainTree != null;
        this.mainTree = mainTree;
    }

    @Override
    public Node<T> getParent() {
        if (parent == null){
            throw new IllegalStateException("No parent set.");
        }
        return parent;
    }

    @Override
    public void setParent(Node<T> parent) {
        assert parent != null;
        this.parent = parent;
    }

    @Override
    public List<Node<T>> getNodes() {
        return new ArrayList<>(nodes);
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object o) {
        return isEqual(o);
    }

    @Override
    public int hashCode() {
        return getHashCode();
    }

    @Override
    public void addNodeToTree(Node<T> node) throws TreeInsertionException {
        nodes.add(node);
    }

    @Override
    public void removeNodesFromTree(Node<T> node) throws TreeRemovalException {
        nodes.remove(node);
    }

    @Override
    public void fireTreeModelListenersForInsertion(Node<T> node) throws NodeNotFoundException {
        if (mainTree == null){
            throw new IllegalStateException("MainTree not set. Please call setMainTree() first.");
        }
        mainTree.fireTreeModelListenersForInsertion(node);
    }

    @Override
    public void fireTreeModelListenersForRemoval(Node<T> node) throws NodeNotFoundException {
        if (mainTree == null){
            throw new IllegalStateException("MainTree not set. Please call setMainTree() first.");
        }
        mainTree.fireTreeModelListenersForRemoval(node);
    }
}
