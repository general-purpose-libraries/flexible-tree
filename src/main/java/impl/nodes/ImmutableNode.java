package impl.nodes;

import api.ChildTree;
import api.Node;
import api.TreeValue;
import api.utils.NodePath;
import impl.nodepaths.ImmutableNodePath;

import javax.swing.tree.TreeNode;

/**
 * Implements a node which value can not be changed
 */
public class ImmutableNode<T> implements Node<T> {
    private final TreeValue<T> value;
    private final Node<T> parent;
    private final ChildTree<T> childTree;
    private final NodePath<T> nodePath;

    public ImmutableNode(TreeValue<T> value, ChildTree<T> childTree, NodePath<T> nodePath) {
        assert childTree != null;
        assert value != null;
        assert nodePath != null;

        this.value = value;
        this.value.setOwnerNode(this);
        this.childTree = childTree;
        this.childTree.setParent(this);
        this.parent = null;
        this.nodePath = nodePath;
    }

    public ImmutableNode(TreeValue<T> value, Node<T> parent, ChildTree<T> childTree, NodePath<T> nodePath) {
        assert childTree != null;
        assert value != null;
        assert parent != null;
        assert nodePath != null;

        this.value = value;
        this.childTree = childTree;
        this.childTree.setParent(this);
        this.parent = parent;
        this.nodePath = nodePath;
    }

    @Override
    public TreeValue<T> getValue() {
        return value;
    }

    @Override
    public ChildTree<T> getChildTree() {
        return childTree;
    }

    @Override
    public NodePath<T> getNodePath() {
        return new ImmutableNodePath<>(nodePath.getNodes());
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object o) {
        return isEqual(o);
    }

    @Override
    public int hashCode() {
        return getHashCode();
    }

    @Override
    public TreeNode getParent() {
        return parent;
    }

    @Override
    public String toString() {
        return "ImmutableNode{" +
                "value=" + value +
                '}';
    }
}
