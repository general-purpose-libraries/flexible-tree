package impl.treevalues;

import api.TreeValue;

/**
 * Created by chief on 06.07.17.
 */
public class IntegerTreeValue extends TreeValue<Integer> {

    public IntegerTreeValue(Integer value) {
        super(value);
    }

    @Override
    public String getValueString() {
        return String.valueOf(getValue());
    }
}
