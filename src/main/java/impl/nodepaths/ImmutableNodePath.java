package impl.nodepaths;

import api.utils.NodePath;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedDeque;

/**
 * This is a node path which values can not be changed
 */
public class ImmutableNodePath<T> implements NodePath<T>{

    private final ConcurrentLinkedDeque<T> values = new ConcurrentLinkedDeque<>();

    public ImmutableNodePath() {}

    public ImmutableNodePath(T value){
        assert value != null;
        this.values.add(value);
    }

    public ImmutableNodePath(List<T> values) {
        assert values != null;
        this.values.addAll(values);
    }

    public ImmutableNodePath(List<T> values, T value) {
        assert values != null;
        assert value != null;
        this.values.addAll(values);
        this.values.add(value);
    }

    @Override
    public List<T> getNodes() {
        return new ArrayList<>(values);
    }

    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object o) {
        return isEqual(o);
    }

    @Override
    public int hashCode() {
        return getHashCode();
    }

    @Override
    public String toString() {
        return getAsString();
    }
}
