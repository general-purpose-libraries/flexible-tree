package api;

import java.util.List;
import java.util.Objects;

/**
 * Represents hierarchical collection of child nodes of a Tree
 */
public interface ChildTree<T>{
    Node<T> getParent();
    void setParent(Node<T> parent);
    List<Node<T>> getNodes();

    default boolean isEqual(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChildTree<?> that = (ChildTree<?>) o;
        return Objects.equals(getNodes(), that.getNodes());
    }

    default int getHashCode(){
        return Objects.hash(getNodes());
    }
}
