package api;

import api.utils.NodePath;
import exception.NodeNotFoundException;
import impl.treelinearizers.CompactTreeLinearizer;

import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Represents a hierarchical collection of nodes
 */
public interface Tree<V> extends TreeModel{

    Node<V> getTreeRoot();
    Collection<TreeModelListener> getListeners();
    default void fireTreeModelListenersForChange(Node<V> node) throws NodeNotFoundException {
        TreeModelEvent treeModelEvent = new TreeModelEvent(node, node.getNodePath().toTreePath());
        getListeners().stream().forEach(treeModelListener -> treeModelListener.treeNodesChanged(treeModelEvent));
    }

    @SuppressWarnings("SimplifyStreamApiCallChains")
    default void fireTreeModelListenersForInsertion(Node<V> node) throws NodeNotFoundException {
        TreeModelEvent treeModelEvent = new TreeModelEvent(node, node.getNodePath().toTreePath());
        getListeners().stream().forEach(treeModelListener -> treeModelListener.treeNodesInserted(treeModelEvent));
    }

    @SuppressWarnings("SimplifyStreamApiCallChains")
    default void fireTreeModelListenersForRemoval(Node<V> node) throws NodeNotFoundException {
        TreeModelEvent treeModelEvent = new TreeModelEvent(node, node.getNodePath().toTreePath());
        getListeners().stream().forEach(treeModelListener -> treeModelListener.treeNodesRemoved(treeModelEvent));
    }

    default Collection<NodePath<V>> linearizeTree(){
        return new CompactTreeLinearizer<V>().linearize(this);
    }

    default TreeValue<V> getChildFromNode(Node<V> parent, int index) throws NodeNotFoundException {
        assert parent != null;
        assert index >= 0;

        List<Node<V>> children = parent.getChildTree().getNodes();
        if (children.isEmpty() || index >= children.size()){
            throw new NodeNotFoundException(String.format("Child node index %s is invalid for parent node '%s'",
                    String.valueOf(index), parent.getValue().toString()));
        }
        return children.get(index).getValue();
    }

    default int getIndexOfChildNode(Node<V> parent, Node<V> child) throws NodeNotFoundException {
        assert parent != null;
        assert child != null;

        List<Node<V>> children = parent.getChildTree().getNodes();
        if (!children.contains(child)){
            throw new NodeNotFoundException(String.format("Node '%s' not found in parent node '%s'",
                    child.getValue().toString(), parent.getValue().toString()));
        }
        return children.indexOf(child);
    }

    default boolean isEqual(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tree<?> that = (Tree<?>) o;
        return getTreeRoot().isEqual(that);
    }

    default int getHashCode(){
        return Objects.hash(getTreeRoot().getHashCode());
    }

    @SuppressWarnings("unchecked")
    @Override
    default void valueForPathChanged(TreePath treePath, Object newValue){
        assert treePath != null;
        assert newValue != null;

        Object node = treePath.getLastPathComponent();
        if (!(node instanceof Node)){
            throw new IllegalArgumentException(String.format("Expected '%s', but found '%s'", Node.class.getSimpleName(), node.getClass().getSimpleName()));
        }
        try {
            fireTreeModelListenersForChange((Node<V>) node);
        } catch (NodeNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    default Object getRoot(){
        return getTreeRoot().getValue();
    }

    @SuppressWarnings("unchecked")
    @Override
    default Object getChild(Object parent, int index){
        assert parent != null;
        assert index >= 0;

        try {
            return getChildFromNode((Node<V>) parent, index);
        } catch (NodeNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    default int getChildCount(Object o){
        assert o != null;
        return getTreeRoot().getChildCount();
    }

    @SuppressWarnings("unchecked")
    @Override
    default int getIndexOfChild(Object parent, Object child){
        assert parent != null;
        assert child != null;

        if (!(parent instanceof Node)){
            throw new IllegalArgumentException("Parent have to be an instance of Node");
        }
        if (!(child instanceof Node)){
            throw new IllegalArgumentException("Child have to be an instance of Node");
        }
        try {
            return getIndexOfChildNode((Node<V>) parent, (Node<V>) child);
        } catch (NodeNotFoundException e) {
            return -1;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    default boolean isLeaf(Object object){
        if (!(object instanceof TreeValue)){
            throw new IllegalArgumentException(String.format("Parent have to be an instance of %s", TreeValue.class.getSimpleName()));
        }
        TreeValue<V> node = (TreeValue<V>) object;
        if (!node.getOwnerNode().isLeaf()) return false;
        Map<NodePath<Node<V>>, ChildTree<V>> nodePaths = new CompactTreeLinearizer<V>().buildNodePaths(this);
        return nodePaths.keySet().parallelStream().anyMatch(path -> path.getLastNode().isEqual(node));
    }
}
