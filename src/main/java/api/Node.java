package api;

import api.utils.NodePath;
import exception.NodeNotFoundException;

import javax.swing.tree.TreeNode;
import java.util.*;

/**
 * Represents a node of a Tree
 */
public interface Node<V> extends TreeNode, Comparable<Node<V>>{
    interface NodeAction<V> {
        void apply(Node<V> node);
    }

    TreeValue<V> getValue();
    ChildTree<V> getChildTree();
    NodePath<V> getNodePath();

    default void doPerform(NodeAction<V> action){
        assert action != null;
        action.apply(this);
    }

    default boolean isEqual(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node<?> that = (Node<?>) o;
        return Objects.equals(getValue(), that.getValue()) &&
                Objects.equals(getChildTree(), that.getChildTree());
    }

    default int getHashCode(){
        return Objects.hash(getValue(), getChildTree());
    }

    default int compareTo(Node<V> node) {
        assert node != null;
        if (equals(node)){
            return 0;
        }
        int comparationResult = Integer.compare(getHashCode(), node.getHashCode());
        if (comparationResult != 0){
            return comparationResult;
        }
        return -1;
    }

    default Node<V> getChildAtPosition(int index) throws NodeNotFoundException {
        List<Node<V>> nodes = getChildTree().getNodes();
        if (nodes.isEmpty() || index >= nodes.size()){
            throw new NodeNotFoundException(String.format("Child node index %s is invalid for node '%s'",
                    String.valueOf(index), getValue().toString()));
        }
        return nodes.get(index);
    }

    default int getIndexOfChild(TreeValue<V> child) throws NodeNotFoundException {
        List<Node<V>> children = getChildTree().getNodes();
        List<TreeValue<V>> childrenValues = new ArrayList<>();
        children.forEach(node -> childrenValues.add(node.getValue()));
        if (!children.contains(child)){
            throw new NodeNotFoundException(String.format("Node '%s' not found in node '%s'",
                    child.toString(), getValue().toString()));
        }
        return childrenValues.indexOf(child);
    }

    @Override
    default TreeNode getChildAt(int index){
        try {
            return (TreeNode) getChildAtPosition(index).getValue();
        } catch (NodeNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    default int getChildCount(){
        return getChildTree().getNodes().size();
    }

    @SuppressWarnings("unchecked")
    @Override
    default int getIndex(TreeNode treeNode){
        if (!(treeNode instanceof Node)){
            throw new IllegalArgumentException("Child node have to be an instance of Node");
        }
        try {
            return getIndexOfChild((TreeValue<V>) treeNode);
        } catch (NodeNotFoundException e) {
            return -1;
        }
    }

    @Override
    default Enumeration children(){
        return new Enumeration() {
            Iterator<Node<V>> children = getChildTree().getNodes().iterator();

            @Override
            public boolean hasMoreElements() {
                return children.hasNext();
            }

            @Override
            public Object nextElement() {
                return children.next().getValue();
            }
        };
    }

    @Override
    default boolean getAllowsChildren(){
        return true;
    }

    @Override
    default boolean isLeaf(){
        return getChildTree().getNodes().isEmpty();
    }
}
