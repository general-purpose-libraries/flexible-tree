package api.utils;

import javax.swing.tree.TreePath;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Represents the complete path of a node in a Tree
 */
public interface NodePath<T> extends Comparable<NodePath<T>>{
    List<T> getNodes();

    default TreePath toTreePath(){
        return new TreePath(getNodes().toArray());
    }

    default T getLastNode(){
        List<T> nodes = getNodes();
        return nodes.get(nodes.size()-1);
    }

    default String getAsString(){
        return String.format("%s{%s}", this.getClass().getSimpleName(), getNodes());
    }

    default int compareTo(NodePath<T> node) {
        assert node != null;
        if (equals(node)){
            return 0;
        }
        int comparationResult = Integer.compare(getHashCode(), node.getHashCode());
        if (comparationResult != 0){
            return comparationResult;
        }
        return -1;
    }

    default boolean isEqual(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NodePath<?> that = (NodePath<?>) o;
        if (this.getNodes().size() != that.getNodes().size()){
            return false;
        }
        return Objects.equals(this.getNodes(), that.getNodes());
    }

    default int getHashCode(){
        final AtomicInteger result = new AtomicInteger(1);
        getNodes().parallelStream().forEach(t -> {
            final int hash = t.hashCode();
            result.getAndUpdate(l -> 37 * l + hash);
        });

        return result.get();
    }
}
