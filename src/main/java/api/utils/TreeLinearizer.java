package api.utils;

import api.ChildTree;
import api.Node;
import api.Tree;
import impl.nodepaths.ImmutableNodePath;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.*;
import java.util.stream.Collector;
import java.util.stream.Stream;

/**
 * Transforms a tree into a collection of paths of its children
 */
public interface TreeLinearizer<NODEVALUE, TARGETTYPE> {
    interface NodeConverter<NODEVALUE, TARGETTYPE> {
        NodePath<TARGETTYPE> convert(Map.Entry<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>> nodePath);
    }

    Predicate<Map.Entry<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>>> getFilter();
    NodeConverter<NODEVALUE, TARGETTYPE> getConverter();

    default Collection<NodePath<TARGETTYPE>> linearize(Tree<NODEVALUE> tree){
        assert tree != null;
        assert getConverter() != null;

        Map<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>> pathsFound = buildNodePaths(tree);
        final Collection<NodePath<TARGETTYPE>> nodePaths = new ConcurrentSkipListSet<>();
        Set<Map.Entry<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>>> entries = pathsFound.entrySet();
        entries.parallelStream().forEach(nodePathTreeEntry -> {
            nodePaths.add(getConverter().convert(nodePathTreeEntry));
        });
        return nodePaths;
    }

    default Map<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>> buildNodePaths(Tree<NODEVALUE> tree) {
        assert tree != null;
        assert getFilter() != null;

        Map<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>> pathsFound = new ConcurrentHashMap<>();
        Map<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>> currentNodePaths = new ConcurrentHashMap<>();
        Map<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>> nextNodePaths = currentNodePaths;
        ImmutableNodePath<Node<NODEVALUE>> nodePath = new ImmutableNodePath<>(tree.getTreeRoot());
        ChildTree<NODEVALUE> childTree = tree.getTreeRoot().getChildTree();
        currentNodePaths.put(nodePath, childTree);
        pathsFound.put(nodePath, childTree);

        while (!nextNodePaths.isEmpty()) {
            Set<Map.Entry<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>>> entries = currentNodePaths.entrySet();
            Stream<Map.Entry<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>>> entryStream = entries.parallelStream();
            nextNodePaths = entryStream.collect(new Collector<Map.Entry<NodePath<Node<NODEVALUE>>,ChildTree<NODEVALUE>>, ConcurrentHashMap<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>>, Map<NodePath<Node<NODEVALUE>>,ChildTree<NODEVALUE>>>() {
                final ConcurrentHashMap<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>> map = new ConcurrentHashMap<>();

                @Override
                public Supplier<ConcurrentHashMap<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>>> supplier() {
                    return () -> map;
                }

                @Override
                public BiConsumer<ConcurrentHashMap<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>>, Map.Entry<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>>> accumulator() {
                    return (path, treeEntry) -> {
                        List<Node<NODEVALUE>> nodeList = treeEntry.getKey().getNodes();
                        ChildTree<NODEVALUE> childTree = treeEntry.getValue();
                        List<Node<NODEVALUE>> nodes = childTree.getNodes();
                        Stream<Node<NODEVALUE>> nodeStream = nodes.parallelStream();
                        nodeStream.forEach(node -> {
                            ChildTree<NODEVALUE> childTree1 = node.getChildTree();
                            path.put(new ImmutableNodePath<>(nodeList, node), childTree1);
                        });
                    };
                }

                @Override
                public BinaryOperator<ConcurrentHashMap<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>>> combiner() {
                    return (path1, path2) -> {
                        path1.putAll(path2);
                        return path1;
                    };
                }

                @Override
                public Function<ConcurrentHashMap<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>>, Map<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>>> finisher() {
                    return map -> map;
                }

                @Override
                public Set<Characteristics> characteristics() {
                    return new HashSet<>(Arrays.asList(
                            Characteristics.UNORDERED,
                            Characteristics.IDENTITY_FINISH,
                            Characteristics.CONCURRENT
                    ));
                }
            });

            pathsFound.putAll(nextNodePaths);
            currentNodePaths = nextNodePaths;
        }
        Map<NodePath<Node<NODEVALUE>>, ChildTree<NODEVALUE>> result = new ConcurrentHashMap<>();
        pathsFound.entrySet().parallelStream().filter(getFilter()).forEach(treeEntry -> result.put(treeEntry.getKey(), treeEntry.getValue()));
        return result;
    }
}
