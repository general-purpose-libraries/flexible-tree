package api.utils;

import api.ChildTree;
import api.Node;
import exception.NodeNotFoundException;
import exception.TreeInsertionException;
import exception.TreeRemovalException;

/**
 * Represents a tree which can be modified
 */
public interface MutableChildTree<T> extends ChildTree<T> {
    void addNodeToTree(Node<T> node) throws TreeInsertionException;
    void removeNodesFromTree(Node<T> node) throws TreeRemovalException;
    void fireTreeModelListenersForInsertion(Node<T> changedNode) throws NodeNotFoundException;
    void fireTreeModelListenersForRemoval(Node<T> node) throws NodeNotFoundException;

    default void addNode(Node<T> node) throws TreeInsertionException{
        assert node != null;
        addNodeToTree(node);

        try {
            fireTreeModelListenersForInsertion(node);
        } catch (NodeNotFoundException e) {
            throw new TreeInsertionException(e);
        }
    }

    default void removeNode(Node<T> node) throws TreeRemovalException {
        assert node != null;
        removeNodesFromTree(node);

        try {
            fireTreeModelListenersForRemoval(node);
        } catch (NodeNotFoundException e) {
            throw new TreeRemovalException(e);
        }
    }
}
