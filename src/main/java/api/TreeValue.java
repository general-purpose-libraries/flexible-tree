package api;

import java.util.Objects;

/**
 * Created by chief on 06.07.17.
 */
public abstract class TreeValue<V> {
    private final V value;
    private Node<V> ownerNode;

    protected TreeValue(V value) {
        assert value != null;
        this.value = value;
    }

    public V getValue(){
        return value;
    }

    public Node<V> getOwnerNode() {
        if (ownerNode == null){
            throw new IllegalStateException("Owner node not set.");
        }
        return ownerNode;
    }

    public void setOwnerNode(Node<V> ownerNode) {
        assert ownerNode != null;
        this.ownerNode = ownerNode;
    }

    public abstract String getValueString();

    @Override
    public String toString() {
        return getValueString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreeValue<?> treeValue = (TreeValue<?>) o;
        return Objects.equals(getValue(), treeValue.getValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getValue());
    }
}
