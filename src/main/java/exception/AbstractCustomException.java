package exception;

/**
 * This class simplifies implementation of custom exceptions
 */
public abstract class AbstractCustomException extends Exception {
    private final String message;

    public AbstractCustomException(Exception exception) {
        assert exception != null;
        this.message = exception.getMessage();
        if (exception.getStackTrace().length == 0){
            this.setStackTrace(Thread.currentThread().getStackTrace());
        }
        else {
            this.setStackTrace(exception.getStackTrace());
        }
    }

    public AbstractCustomException(String message) {
        assert message != null;
        this.message = message;
        this.setStackTrace(Thread.currentThread().getStackTrace());
    }
}
