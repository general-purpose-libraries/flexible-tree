package exception;

/**
 * Thrown when nodes could not be added to a tree
 */
public class TreeInsertionException extends AbstractCustomException {
    public TreeInsertionException(Exception exception) {
        super(exception);
    }

    public TreeInsertionException(String message) {
        super(message);
    }
}
