package exception;

/**
 * Created by chief on 04.07.17.
 */
public class TreeRemovalException extends AbstractCustomException {
    public TreeRemovalException(Exception exception) {
        super(exception);
    }

    public TreeRemovalException(String message) {
        super(message);
    }
}
