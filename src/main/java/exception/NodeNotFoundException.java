package exception;

/**
 * Created by chief on 04.07.17.
 */
public class NodeNotFoundException extends AbstractCustomException{
    public NodeNotFoundException(Exception exception) {
        super(exception);
    }

    public NodeNotFoundException(String message) {
        super(message);
    }
}
